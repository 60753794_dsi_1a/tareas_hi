# tareas del curso de herramientas informaticas



## Estudiante: Jack Anderson Sucapuca Zea
## tareas para casa
- [Tarea4](Tareas/4.Trabajo+con+Celdas+Filas+Columna.xlsx)
- [Tarea5](Tareas/5.Formato+de+Textos.xlsx)
- [Tarea6](<Tareas/6.Formato+Numericos J.xlsx>)
- [Tarea7](<Tareas/Copia de 7.Formatos+de+Tablas+y+Estilos+de+Celdas J (1).xlsx>)
- [Tarea8](<Tareas/Copia de 8.Formulas+Basicas J (1).xlsx>)
- [Tarea 10](<Tareas/Copia de 10.Formulas+y+Funciones+con+Fechas J (1).xlsx>)
- [Tarea 11](Tareas/11.Graficos+de+una+Variable.xlsx)
- [Tarea12](Tareas/12.Graficos+de+dos+o+mas+variables.xlsx)
- [Tarea13](Tareas/13.Teoria+Concepto+Base+Datos.xlsx)
- [Tarea14](<Tareas/14.Orden+de+Datos (1).xlsx>)
- [Tarea17](<Tareas/17.Macros+con+Graficos J.xlsm>)
## Prácticas de Excel
- [Práctica1](<Tareas/EJERCICIO  N1 PRESUPUESTO DE VENTAS (1) (1).xlsx>)
- [Práctica2](<Tareas/EJERCICIO N2 FACTURAS (2).xlsm>)
- [Práctica3](<Tareas/EJERCICIO N 3 MOVIMIENTOS DE ANIMALES (1).xlsx>)
- [Práctica4](<Tareas/EJERCICIO 4 GRÁFICOS (1).xlsx>)
- [Práctica5](<Tareas/Sueldos N5.xlsx>)
- [Práctica6](<Tareas/EJERCICIO N6 LISTA DE PRECIOS Y CONTROL DE STOCK (1).xlsx>)
- [Práctica7](<Tareas/SUBTOTALES N7.xlsx>)
## Práctica Calificada
- [PRÁCTICA EXCEL](<Tareas/Practica de Laboratorio_Sz.xlsm>)
## laragon
- http://ander_proyecto.test/
